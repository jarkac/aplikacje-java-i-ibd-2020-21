package wyklad1;

public class SilnikHybrydowy extends Naped {

	private int zuzycieEnergii;

	public SilnikHybrydowy(int zuzycieEnergii) {
		this.zuzycieEnergii = zuzycieEnergii;
	}
	
	public double dajChwiloweZyzucieEnergii(int rpm) {
		return rpm*zuzycieEnergii;
	}

}
