package wyklad1;

public class Run {

	public static void main(String[] args) {

		
		{
			Samochod s1 = new Samochod( "Ford", "Mondeo", 7);
			s1.zamontujNaped( new SilnikSpalinowy( 1999, 7.5 ) );

			s1.wlaczSilnik();
			System.out.println( "Spalanie chwilowe "+s1.dajChwiloweZyzucieEnergii(3000)+"W/100km");
			s1.wylaczSilnik();

			Samochod s2 = new Samochod("Ford", "Focus", 8);
			s2.zamontujNaped( new SilnikHybrydowy( 1 ) );
			s2.wlaczSilnik();
			System.out.println( "Spalanie chwilowe "+s2.dajChwiloweZyzucieEnergii(3000)+"W/100km");
			s2.wylaczSilnik();

			s1.pokazInformacje();
			s2.pokazInformacje();

			s1.setMarka("Honda");
			s1.pokazInformacje();
			
			Samochod samochodZSilnikiemSpalinowym = s1;
			s1 = null;
			samochodZSilnikiemSpalinowym.wlaczSilnik();
		}
		
		
		
	}

}
