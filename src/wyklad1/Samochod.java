package wyklad1;

public class Samochod {
	private static final String ZMIANA_MARKI_NIE_JEST_MOZLIWA = "Zmiana marki nie jest mozliwa";
	private String marka;
	public String model;
	public int kolor;
	public Naped naped;
	
	public Samochod(String marka, String model, int kolor) {
		this.marka = marka;
		this.model = model;
		this.kolor = kolor;
	}

	public void wlaczSilnik() {
		naped.setSilnikWlaczony(true);
		System.out.println("Silnik wlaczony");
	}

	public void wylaczSilnik() {
		naped.setSilnikWlaczony(false);
		System.out.println("Silnik wylaczony");
	}

	public double dajChwiloweZyzucieEnergii(int rpm) {
		return naped.dajChwiloweZyzucieEnergii(rpm);
	}

	public void zamontujNaped(Naped montowanySilnik) {
		naped = montowanySilnik;
		
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String nowaMarka) {
		if (marka == null) {
			marka = nowaMarka;
		} else {
			System.err.println(ZMIANA_MARKI_NIE_JEST_MOZLIWA);
		}
	}
	
	public void pokazInformacje() {
		System.out.println("Samochod marki "+this.getMarka()+" model "+this.model+" o kolorze id="+this.kolor );		
	}
}
