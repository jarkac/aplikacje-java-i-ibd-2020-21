package wyklad1;

public class SilnikSpalinowy extends Naped {

	private int pojemnoscSilnika;
	private double spalaniePrzy2000;

	public SilnikSpalinowy( int pojemnosc, double spalaniePrzy2000 ) {
		this.pojemnoscSilnika = pojemnosc;
		this.spalaniePrzy2000 = spalaniePrzy2000;
	}

	public double dajChwiloweZyzucieEnergii(int rpm) {
		double chwiloweZuzycie = rpm*spalaniePrzy2000/2000;
		return chwiloweZuzycie;
	}


}
