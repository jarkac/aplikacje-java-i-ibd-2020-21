package wyklad4;

public class KlasyEksperymenty {
	public static void main(String[] args) throws Exception {
		FiguraGeometryczna f = new Kwadrat(5);
		System.out.println( f+" o polu "+f.obliczPole() );
		f = new Prostokat(5,10);
		System.out.println( f+" o polu "+f.obliczPole() );
		DajacyCien cienie[] = new DajacyCien[] {
				new Kwadrat(6),
				new Prostokat(3, 4),
				new Student("Ala")
		};
		double wszystkieCienieRazem = 0;
		for (DajacyCien cien:cienie) {
			wszystkieCienieRazem += cien.najwiekszyCien();
		}
		System.out.println( "Przy pomocy tych obiektow zaslonimy max "+wszystkieCienieRazem+" m^2");
	}
}
