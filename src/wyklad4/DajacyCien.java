package wyklad4;

public interface DajacyCien {
	double najmniejszyCien();
	double najwiekszyCien();
//	double cienPodKatem( double kat );
	default double cienPodKatem( double kat ) {
		return najwiekszyCien()*kat/90;
	}
}
