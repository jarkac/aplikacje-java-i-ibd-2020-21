package wyklad4;

public class Prostokat extends FiguraGeometryczna implements DajacyCien {

	private double[] boki;

	public Prostokat( double a, double b ) {
		this.boki = new double[] {a,b};
	}
	
	@Override
	public double obliczPole() {
		return boki[0]*boki[1];
	}

	@Override
	public String nazwaFigury() {
		return "prostokat";
	}

	@Override
	public double najmniejszyCien() {
		return 0;
	}

	@Override
	public double najwiekszyCien() {
		return obliczPole();
	}

}
