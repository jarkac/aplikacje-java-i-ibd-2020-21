package wyklad4;

public abstract class FiguraGeometryczna {
	public abstract double obliczPole();
	public abstract String nazwaFigury();
	@Override
	public String toString() {
		return "to jest FiguraGeometryczna, a dokladnie "+nazwaFigury();
	}
}
