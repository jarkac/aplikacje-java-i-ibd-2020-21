package wyklad4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringEksperymenty {
	private static final String EMAIL_REGEX = "[a-z]+@[a-z\\.]+[a-z]";

	public static void main(String[] args) {
		String tekst = "Java";
		char znak = tekst.charAt(0);
		System.out.println(znak);
		String t1 = "Ala";
		String t2 = "Ola";
		System.out.println( t1.compareTo(t2) );
		
		System.out.println( tekst.concat(" jest OK") );
		System.out.println( tekst.startsWith("ava",1));
		
		Jakas j = new Jakas( 6 );
		System.out.println("obiekt j = "+j);

		Jakas k = new Jakas( 6 );
		System.out.println( j.equals(k) );
		
		// C: printf()
		int x = 5;
		int y = 7;
		String informacja = "tajna";
		// a=5, b=7, to jest informacja: tajna
		String t3a = "a="+x+", b="+y+", to jest informacja: "+informacja; 
		String t3b = String.format("a=%d, b=%d, to jest informacja: %s", x, y, informacja);
		double d = Math.PI;
		System.out.println( "pi = "+d );
		System.out.println( String.format("pi = %+40.15f", d) );
		
		String t4 = "abc";
		t4 = t4.toUpperCase();
		System.out.println(t4);
		
		String t5 = "abc";
		String t6 = "ab";
		t6 = t6+"c";
		System.out.println( t5.equals(t6) );
		
		String t7 = "1  ,2,,,,,3;4, 5,10";
		String t7Kawalki[] = t7.split(" *[,;]+ *");
		System.out.println("length = "+t7Kawalki.length);
		for (String s:t7Kawalki) {
			System.out.println("\t["+s+"]");
		}
		
		String t8 = "ala@ma.kota.pl.";
		System.out.println( t8+" jest poprawnym adresem email: "+t8.matches(EMAIL_REGEX));
		
		Pattern pat = Pattern.compile("abc[1-9]+");
		Matcher m = pat.matcher("abc123 abc455, 6");
		while (m.find()) {
			System.out.println( m.group() );
		}
	}
}
