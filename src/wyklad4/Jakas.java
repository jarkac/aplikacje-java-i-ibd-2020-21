package wyklad4;

public class Jakas {

	private int wart;

	public Jakas(int wart) {
		this.wart = wart;
	}

	@Override
	public String toString() {
		
		return "obiekt klasy J( wart="+wart+" )";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + wart;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jakas other = (Jakas) obj;
		if (wart != other.wart)
			return false;
		return true;
	}
	
	
}
