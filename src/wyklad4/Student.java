package wyklad4;

public class Student implements DajacyCien {

	private static final int ROZMIAR_BUTA = 42;
	private static final double MAKS_CIEN_STUDENTA = 150;

	public Student(String string) throws Exception {
		if (string == null) {
			throw new Exception("Nie podano imienia!");
		}
	}

	@Override
	public double najmniejszyCien() {
		return ROZMIAR_BUTA*ROZMIAR_BUTA;
	}

	@Override
	public double najwiekszyCien() {
		return MAKS_CIEN_STUDENTA;
	}

}
