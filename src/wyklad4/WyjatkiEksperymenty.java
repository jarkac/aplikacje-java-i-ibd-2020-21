package wyklad4;

import java.io.FileNotFoundException;

public class WyjatkiEksperymenty {
	public static void main(String[] args) {
		try {
			met1();
		} catch (NaszException e) {
			System.out.println("Inny wyjatek: "+e.getLocalizedMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("To nie byl poprawny indeks: "+e.getLocalizedMessage());
			return;
		} finally {
			System.out.println("Zawsze!");
		}
		System.out.println("xxx3");
	}

	private static void met1() throws NaszException {
		met2();
		System.out.println("xxx2");
	}

	private static void met2() throws NaszException {
		int[] a = {1,2,3};
		if (a[0]==1) {
			throw new NaszException("Rzucamy wlasny wyjatek");
		}
		System.out.println(a[5]); // throw Exception (ArrayIndexOutOfBoundsException)
		System.out.println("???");
		System.out.println("xxx");
	}
}
