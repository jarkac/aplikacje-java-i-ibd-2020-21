package wyklad4;

public class Kwadrat extends FiguraGeometryczna implements DajacyCien {

	private double bok;

	public Kwadrat( double a ) {
		this.bok = a;
	}
	
	@Override
	public double obliczPole() {
		return bok*bok;
	}

	@Override
	public String nazwaFigury() {
		return "kwadrat";
	}

	@Override
	public double najmniejszyCien() {
		return 0;
	}

	@Override
	public double najwiekszyCien() {
		return obliczPole();
	}

}
