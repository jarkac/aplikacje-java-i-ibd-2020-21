package wyklad5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

class Okno extends JFrame {
	final static String tekst = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at bibendum arcu. Mauris sed est enim. Vivamus in nisl at libero vehicula dapibus vitae quis massa. Praesent pellentesque mi sit amet tincidunt sodales. Quisque volutpat non dui sit amet gravida. Nulla aliquam ex ac nulla porttitor pellentesque. Nam placerat egestas erat in egestas.\n" + 
			"\n" + 
			"Aenean bibendum bibendum facilisis. Ut at arcu ac lectus viverra imperdiet. Sed faucibus ac nulla in dictum. Sed a ipsum posuere, dignissim mauris at, viverra erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel eros et purus elementum feugiat. Pellentesque rutrum efficitur magna sit amet posuere. Mauris egestas finibus porttitor. Donec ex ligula, tempus at nibh eu, sodales interdum lorem. Quisque accumsan diam eu sapien vulputate, sed accumsan quam consequat. Nam sed purus at tortor gravida auctor.\n" + 
			"\n" + 
			"Mauris mattis laoreet bibendum. Proin efficitur leo vel ante sollicitudin, vitae facilisis mi facilisis. Aliquam erat volutpat. Praesent maximus urna nibh. Aenean quis ultrices elit. Sed nec neque egestas, dapibus orci ac, varius libero. Integer hendrerit tempus consectetur. Etiam efficitur eros vel molestie sagittis. Nullam rutrum placerat ante feugiat ullamcorper. Fusce interdum ipsum id leo tempor auctor. Ut et aliquet dui. In ac semper risus.";
	private JLabel licznik;
	private JButton przyciskPlus;
	Okno() {
		createMenu();

		Container mainContainer = this.getContentPane();
		mainContainer.setLayout( new BorderLayout() );
		JPanel panel = new JPanel();
		mainContainer.add( panel, BorderLayout.SOUTH);
		
		mainContainer.add( new JScrollPane(new JTextArea()), BorderLayout.CENTER );
		
		
		Container c = panel;
		panel.setPreferredSize( new Dimension(100,400));
		c.setLayout( new FlowLayout( FlowLayout.LEFT ) );
		JTextField nowyWyraz = new JTextField( 20 );
		c.add(nowyWyraz);
		JButton b = new JButton();
		c.add( przyciskPlus = new JButton("Przycisk+") );
		przyciskPlus.addActionListener((e)->{plusLubMinus(true);});
		licznik = new JLabel("0");
		c.add(licznik);
		for (String slowo:tekst.split(" ")) {
			JLabel etykieta;
			c.add( etykieta = new JLabel(slowo+" ") );
			etykieta.addMouseListener( new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					System.out.println(e.getSource());
					c.remove((Component)e.getSource());
					c.validate();
				}
			});
		}
		przyciskPlus.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(Okno.this, "Przycisk kliknieto");

			}

		});
		c.add( b = new JButton("Przycisk-") );
		b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				plusLubMinus(false);
			}
		});
		nowyWyraz.addKeyListener( new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					c.add( new JLabel(nowyWyraz.getText()) );
					c.validate();
					nowyWyraz.setText("");
				}
			}
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar()==' ') {
					c.add( new JLabel(nowyWyraz.getText()) );
					c.validate();
					nowyWyraz.setText("");
				}
			}
		});
		nowyWyraz.getDocument().addDocumentListener( new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				System.out.println(nowyWyraz.getText());
				if (nowyWyraz.getText().length()<3) {
					nowyWyraz.setBackground( Color.red );
				} else {
					nowyWyraz.setBackground( Color.white );
				}
			}

		});
		setSize(400,400);
	}

	private void createMenu() {
		JMenuBar pasekMenu = new JMenuBar();
		setJMenuBar(pasekMenu);
		JMenu menuPlik = new JMenu("Plik");
		pasekMenu.add(menuPlik);
		JMenuItem miKoniec = new JMenuItem("Koniec");
		menuPlik.add(miKoniec);
		miKoniec.addActionListener((e)->{
			System.exit(0);
		});
		JMenu subMenu1 = new JMenu("Podmenu");
		menuPlik.add( subMenu1 );

		JRadioButtonMenuItem wybor[] = new JRadioButtonMenuItem[3];
		subMenu1.add( wybor[0] = new JRadioButtonMenuItem( "Pierwsza" ) );
		wybor[0].setSelected( true );
		subMenu1.add( wybor[1] = new JRadioButtonMenuItem( "Druga" ) );
		subMenu1.add( wybor[2] = new JRadioButtonMenuItem( "Trzecia" ) );
//		ButtonGroup grupaOpcji = new ButtonGroup();
//		grupaOpcji.add( wybor[0] );
//		grupaOpcji.add( wybor[1] );
//		grupaOpcji.add( wybor[2] );
	}

	protected void plusLubMinus(boolean b) {
		int ile = Integer.valueOf(licznik.getText());
		if (b) {
			ile++;
		} else {
			ile--;
		}
		licznik.setText(Integer.toString(ile));
	}
}

public class Run {
	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(()->{
			new Okno().setVisible(true);
		});
	}
}
