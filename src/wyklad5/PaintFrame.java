package wyklad5;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;

public class PaintFrame extends JFrame {
	private ColorButton kolorRysowania;

	public PaintFrame() {
		createGUI();
		setSize(500,500);
		setVisible(true);
	}

	private void createGUI() {
		Container c = getContentPane();
		c.setLayout( new BorderLayout() );
		c.add(createColourPanel(), BorderLayout.SOUTH);
		c.add(new PaintPanel(kolorRysowania), BorderLayout.CENTER);
	}

	private Component createColourPanel() {
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		kolorRysowania = new ColorButton();
		p.add(kolorRysowania);
		Color[] kolory = new Color[] {
				Color.red, Color.black, Color.green, Color.blue
		};
		for (Color kolor:kolory) {
			ColorButton cb;
			p.add( cb = new ColorButton(kolor) );
			cb.addMouseListener( new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					kolorRysowania.setBackground( cb.getBackground() );
					super.mouseClicked(e);
				}
			});
		}
		return p;
	}
}
