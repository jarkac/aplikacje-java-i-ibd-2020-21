package wyklad5;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class ColorButton extends JPanel {
	public ColorButton() {
		this(Color.black);
	}

	public ColorButton(Color color) {
		this.setBackground(color);
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(20,20);
	}
}
