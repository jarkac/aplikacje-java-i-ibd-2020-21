package wyklad5;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

class Punkt {
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Color getColor() {
		return c;
	}

	private int x;
	private int y;
	private Color c;

	Punkt( int x, int y, Color c) {
		this.x = x;
		this.y = y;
		this.c = c;
	}	
}

public class PaintPanel extends JPanel {
	private Component zrodloKoloru;
	List<List<Punkt>> linie = new LinkedList<>();
	List<Punkt> linia = null;
	public PaintPanel(Component zrodloKoloru) {
		this.zrodloKoloru = zrodloKoloru;
		this.addMouseMotionListener( new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (linia == null) {
					linia = new LinkedList<Punkt>();
					linie.add(linia);
				}
				int x = e.getX();
				int y = e.getY();
				linia.add( new Punkt(x,y, zrodloKoloru.getBackground()) );
				PaintPanel.this.repaint();
			}
		});
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				linia = null;
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0,0,getWidth(),getHeight());
		for (List<Punkt> linia:linie) {
			Punkt pierwszyPunkt = null;
			for (Punkt p:linia) {
				g.setColor(p.getColor());
				if (pierwszyPunkt == null) {
					pierwszyPunkt = p;
				}
				g.drawLine(pierwszyPunkt.getX(), pierwszyPunkt.getY(), p.getX(), p.getY());
				pierwszyPunkt = p;
			}
		}
	}
}
