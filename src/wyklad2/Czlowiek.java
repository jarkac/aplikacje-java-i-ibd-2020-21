package wyklad2;

public class Czlowiek {
	Reka lewa, prawa;
	private String imie;

	public Czlowiek( String imie ) {
		this.imie = imie;
	}
	
	public void przedstawSie() {
		System.out.println( "Czesc, jestem "+imie );
	}
}
