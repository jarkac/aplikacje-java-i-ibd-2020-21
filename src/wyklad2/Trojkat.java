package wyklad2;

public class Trojkat {

	private static int ileJuzTrojkatow = 0;
	
	private int bok1;
	private int bok2;
	private int bok3;

	public Trojkat(int a, int b, int c) {
		this.bok1 = a;
		this.bok2 = b;
		this.bok3 = c;
		ileJuzTrojkatow++;
	}

	public Trojkat(int a, int b) {
		this( a, b, (int)Math.sqrt(a*a+b*b) );
	}

	public Trojkat(int abc) {
		this( abc, abc, abc );
	}
	
	public Trojkat() {
		this( 1, 1, 1 );
	}
	
	public void powiekszDlugosciBokowDwukrotnie() {
		this.bok1 *= 2;
		this.bok2 *= 2;
		this.bok3 *= 2;
	}
	
	public static int dajStatystyke() {
//		powiekszDlugosciBokowDwukrotnie();
//		bok1 = 3;
		return ileJuzTrojkatow;
	}

	public static int dajObwod(Trojkat t) {
		return t.bok1 + t.bok2 + t.bok3;
	}

}
