package wyklad2;

public class Student extends Czlowiek {
	private int numerIndeksu;
	
	public final int dajNumerIndeksu() {
		return numerIndeksu;
	}
	
	public Student( String imie, int numerIndeksu ) {
		super(imie);
		this.numerIndeksu = numerIndeksu;
	}
}
