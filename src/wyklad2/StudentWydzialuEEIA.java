package wyklad2;

public class StudentWydzialuEEIA extends Student {

	public static final int NAJNIZSZY_NUMER_INDEKSU_NA_WYDZIALE = 1;
	
	public StudentWydzialuEEIA(String imie, int numerIndeksu) {
		super(imie, numerIndeksu);
		przedstawSie();
		if (numerIndeksu < NAJNIZSZY_NUMER_INDEKSU_NA_WYDZIALE) {
			System.out.println("Mój numer indeksu chyba nie jest poprawny");
		}
	}

	public void liczPierwiastekZMimuns1() {
		System.out.println("i");
	}

	public void przedstawSie() {
		super.przedstawSie();
		final int zapytajNRazy = 5;
		//		super.super.przedstawSie();
		for (int i = 0; i<zapytajNRazy; i++) {
			System.out.println( "Jestem z wydzialu EEIA, a ty?");
		}
	}
}
