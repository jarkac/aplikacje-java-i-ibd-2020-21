package wyklad3;

import java.util.ArrayList;
import java.util.Arrays;

public class Run2 {
	public static void main(String[] args) {
		long t0 = System.currentTimeMillis();
		ArrayList<Integer> a = new ArrayList<>(2000);

		TablicaOZmiennejDlugosci t = new TablicaOZmiennejDlugosci(20000000);
		if (t.length() != 0) {
			System.err.println("Tablica powinna miec dlugosc 0");
		}
		t.add(1);
		for (int i=0;i<300000;i++) {
			a.add( i );
		}
		long tk = System.currentTimeMillis();
		System.out.println("Czas: "+(tk-t0)/1000.0);

		System.out.println(a.get(2));
		System.out.println(a.size());
		int x, y;
		x = 5;
		++x; y = x;
		System.out.println( "x = "+x+", y="+y );

		ArrayList<Object> o = new ArrayList<>();
		o.add("123");
		o.add(o);
		o.add(true);
		o.add(7);
		o.add(7.5);

		ArrayList<String> os = new ArrayList<>();
		os.add("Ala");
		os.add("Ela");
		Object[] osArray = os.toArray();
		String[] osArryaS = os.toArray( new String[0] );
		System.out.println( osArryaS[0].toUpperCase());

		//		int -> Integer -> Number -> Object
		//		double -> Double -> Number -> Object
		//		String -> Object
		Object ob[] = new Object[3];
		ob[0] = 5;
		ob[1] = 5.5;
		ob[2] = "Tekst";
		//		System.out.println( (Double)(ob[0])+1 );
		
		String ob1[] = new String[3];
		ob1[0] = "Ala";
		ob1[1] = "Zzzz";
		ob1[2] = "Tekst";
		Arrays.sort(ob1);
		System.out.println( ob.equals(ob1) );
		System.out.println( Arrays.toString(ob1) );
		
		boolean takieSame = Arrays.equals(ob, ob1);
		System.out.println( "takieSame = "+takieSame );
		
		
	}
}
