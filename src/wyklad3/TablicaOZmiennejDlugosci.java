package wyklad3;

public class TablicaOZmiennejDlugosci {
	private int dane[] = new int[10];
	private int ileElementow = 0;
	
	public TablicaOZmiennejDlugosci( int przewidywanyRozmiarDanych ) {
		dane = new int[przewidywanyRozmiarDanych];
	}
	
	public void add(int nowaWartosc) {
		if (ileElementow == dane.length) {
			int temp[] = new int[2*dane.length];
			for (int i=0;i<ileElementow;i++) { temp[i] = dane[i]; }
			dane = temp;
		}
		dane[ileElementow++] = nowaWartosc;
	}

	public int length() {
		return ileElementow;
	}

	public int get(int index) {
		return dane[index];
	}

}
