package wyklad3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author jarekk
 *
 * 1. pusta lista ma dlugosc zero, sprawdzamy przy pomocy size() [x]
 * 2. po dodaniu elemntu do listy dlugosc jest 1, a element mozna pobrac przy pomocy get(i), dodac add(wartos) [x]
 * 3. po dodadniu kolejnych elementow dlugosc listy sie zwieksza(), ostanio dodany element mozna pobrac jako get(size()-1)
 * 4. element mozna wstawic w dowolne miejsce listy: put(i, wartosc)
 * 5. mozna usunac element z pozycji pierwszej, srodkowej i ostatniej
 * 6. mozna znalezc element podajac wartosc, jesli nie znaleziony to null
 * 7. mozna znalezc element podajac wartosc i indeks poczatkowy, jesli nie znaleziony to null
 */
class ListaJednokierunkowaTest {

	@Test
	void jesliListaJestPustaToMaDlugosc0() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		int iloscElementowListy = l.size();
		assertEquals(0, iloscElementowListy);
	}

	@Test
	void poDodaniuJednegoElementuDlugosc1IMoznaGoPobrac() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.add(17);
		assertEquals(1, l.size());
		assertEquals(17, l.get(0));
	}

	@Test
	void dodawanieWieluElementowZwiekszaListe() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		for (int i=0;i<15;i++) {
			l.add(i+10);
			assertEquals(i+1, l.size());
			assertEquals(i+10, l.get(l.size()-1));
		}
	}


	@Test
	void putNieWstawiaNaNieprawidlowejPozycji() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.put( -1, 0);
		assertEquals(0, l.size());
		l.put( 10, 0);
		assertEquals(0, l.size());
	}

	@Test
	void putWstawiaNaPozycji0() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.put( 0, 7);
		assertEquals(1, l.size());
		assertEquals(7, l.get(0));
		l.put( 0, 8);
		assertEquals(2, l.size());
		assertEquals(8, l.get(0));
	}

	@Test
	void putWstawiaNaPozycji1() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.add(5);
		l.add(6);
		l.put( 1, 7);
		assertEquals(3, l.size());
		assertEquals(7, l.get(1));
	}

	@Test
	void putWstawiaNaPozycjiOstatniej() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.add(5);
		l.add(6);
		l.put(2, 7);
		assertEquals(3, l.size());
		assertEquals(5, l.get(0));
		assertEquals(6, l.get(1));
		assertEquals(7, l.get(2));
	}

	@Test
	void removeNieUsuwaZPustejListy() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.remove(0);
		assertEquals(0, l.size());
	}

	@Test
	void removeUsuwaElementZListy() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.add(5);
		l.add(6);
		l.add(7);
		l.add(8);
		l.remove(0);
		assertEquals(3, l.size());
		assertEquals(6, l.get(0));
		l.remove(1);
		assertEquals(2, l.size());
		assertEquals(8, l.get(1));
	}

	@Test
	void addIRemoveNaZmiane() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		l.add(5);
		assertEquals(1, l.size());
		assertEquals(5, l.get(0));
		l.remove(0);
		assertEquals(0, l.size());

		l.add(5);
		assertEquals(1, l.size());
		assertEquals(5, l.get(0));
		l.remove(0);
		assertEquals(0, l.size());

		l.add(5);
		l.add(6);
		assertEquals(2, l.size());
		assertEquals(5, l.get(0));
		l.remove(0);
		l.remove(0);
		assertEquals(0, l.size());

		l.add(5);
		l.add(6);
		assertEquals(2, l.size());
		assertEquals(5, l.get(0));
		l.remove(1);
		l.remove(0);
		assertEquals(0, l.size());
	}
	
	@Test
	void findZnajdujeElementWgWartosci() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();
		assertNull(l.find(0));
		
		l.add(5);
		l.add(6);
		l.add(7);
		l.add(8);
		assertEquals(0, l.find(5));
		assertEquals(1, l.find(6));
		assertEquals(3, l.find(8));
		
	}

	@Test
	void findZnajdujeElementWgWartosciZaczynajacOdIndeksu() {
		ListaJednokierunkowa l = new ListaJednokierunkowa();		
		l.add(5);
		l.add(6);
		l.add(7);
		l.add(5);
		assertEquals(0, l.find(5,0));
		assertEquals(3, l.find(5,1));
		assertNull(l.find(5,5));
		
	}
}
