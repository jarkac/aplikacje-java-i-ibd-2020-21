package wyklad3;

public class ListaJednokierunkowa {

	private class ListaJednokierunkowaNode {
		int wartosc;
		ListaJednokierunkowaNode nastepny;
	}
	
	private ListaJednokierunkowaNode head = null;
	private ListaJednokierunkowaNode tail = null;
	
	public int size() {
		int wynik = 0;
		ListaJednokierunkowaNode node = head;
		while (node != null) {
			wynik++;
			node = node.nastepny;
		}
		return wynik;
	}

	public void add(int wartosc) {
		ListaJednokierunkowaNode nowyNode = new ListaJednokierunkowaNode();
		nowyNode.wartosc = wartosc;
		nowyNode.nastepny = null;
		if (head == null && tail == null) {
			head = nowyNode;
			tail = nowyNode;
		} else {
			tail.nastepny = nowyNode;
			tail = nowyNode;
		}
	}

	public Integer get(int indeks) {
		int indeksNoda = 0;
		ListaJednokierunkowaNode node = head;
		while (node != null) {
			if (indeksNoda == indeks) {
				return node.wartosc;
			}
			indeksNoda++;
			node = node.nastepny;
		}
		return null;
	}

	public void put(int indeks, int wartosc) {
		ListaJednokierunkowaNode nowyNode = new ListaJednokierunkowaNode();
		nowyNode.wartosc = wartosc;
		nowyNode.nastepny = null;

		if (indeks == 0) {
			nowyNode.nastepny = head;
			head = nowyNode;
			if (tail == null) {
				tail = head;
			}
		} else {
			int indeksNoda = 0;
			ListaJednokierunkowaNode node = head, poprzedniNode = null;
			while (node != null) {
				if (indeksNoda == indeks) {
					nowyNode.nastepny = poprzedniNode.nastepny; 
					poprzedniNode.nastepny = nowyNode;
					return;
				}
				indeksNoda++;
				poprzedniNode = node;
				node = node.nastepny;
			}
			if (indeksNoda == indeks) {
				add( wartosc );
			}
		}
		
	}

	public void remove(int indeks) {
		if (indeks == 0) {
			if (head != null) {
				if (head == tail) {
					tail = null;
				}
				head = head.nastepny;
			}
		} else {
			int indeksNoda = 0;
			ListaJednokierunkowaNode node = head, poprzedniNode = null;
			while (node != null) {
				if (indeksNoda == indeks) {
					poprzedniNode.nastepny = node.nastepny;
					if (tail == node) {
						tail = poprzedniNode;
					}
				}
				indeksNoda++;
				poprzedniNode = node;
				node = node.nastepny;
			}
		}
		
	}

	public Integer find(int wartosc) {
		return find(wartosc, 0);
	}

	public Integer find(int wartosc, int pozycjaPoczatkowa) {
		int indeksNoda = 0;
		ListaJednokierunkowaNode node = head;
		while (node != null) {
			if (node.wartosc == wartosc && indeksNoda >= pozycjaPoczatkowa) {
				return indeksNoda;
			}
			indeksNoda++;
			node = node.nastepny;
		}
		return null;
	}

}
